import Vue from 'vue'
import Router from 'vue-router';
import store from "./store";

Vue.use(Router);

function verificarAcceso(to, from, next) {
    next();
    /*let authUser = store.getters.getNewUser;
    console.log("🚀 ~ file: routes.js ~ line 8 ~ verificarAcceso ~ authUser2", authUser)
    if(authUser.hasOwnProperty('id'))
        next();
    else 
        next(from.fullPath) */
}

export default new Router({
    routes: [
        { 
            path: '/login', 
            component: require('./components/modulos/login/index').default,
            /*beforeEnter: (to, from, next) => {
                let authUser = JSON.parse(sessionStorage.getItem('authUser'));
                console.log("🚀 ~ file: routes.js ~ line 21 ~ authUser", authUser)
                next('/login');
            }*/
        },     
        { 
            path: '/', 
            name: 'login', 
            component: require('./components/modulos/login/index').default,
            beforeEnter: (to, from, next) => {
                let authUser = store.getters.getNewUser;
                if(authUser.hasOwnProperty('id'))
                    next('/dashboard');
                else
                    next('/login');
            }
        },
        /*{ 
            path: '/restorePassword',
            name:'restorePassword.index', 
            component: require('./components/modulos/restorePassword/index').default,
            beforeEnter: (to, from, next) => {
                verificarAcceso(to, from, next);
            }
        },
        { 
            path: '/resetPassword',
            name:'resetPassword.index', 
            component: require('./components/modulos/resetPassword/index').default,
        },*/
        { 
            path: '/dashboard', 
            name:'dashboard.index', 
            component: require('./components/modulos/dashboard/index').default,
            beforeEnter: (to, from, next) => {
                verificarAcceso(to, from, next);
            }
        },
        { 
            path: '/documentos', 
            component: require('./components/modulos/documentos/index').default,
            beforeEnter: (to, from, next) => {
                verificarAcceso(to, from, next);             
            }
        },
        { 
            path: '/procesos', 
            component: require('./components/modulos/procesos/index').default,
            beforeEnter: (to, from, next) => {
                verificarAcceso(to, from, next);             
            }
        },
        { 
            path: '/tipos', 
            component: require('./components/modulos/tipos/index').default,
            beforeEnter: (to, from, next) => {
                verificarAcceso(to, from, next);             
            }
        },     
        { 
            path: '*', 
            component: require('./components/layouts/404').default
        },
    ],
    mode: 'history'
})
