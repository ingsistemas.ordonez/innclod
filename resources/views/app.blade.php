<!DOCTYPE html>
<html lang="es">
@include('sections.head')    
<body class="hold-transition sidebar-mini">
    <div id="app" class="wrapper">
        @if(Auth::check())
            <App ruta="{{route('basepath')}}" :usuario="{{Auth::user()}}"></App>            
        @else
            <Auth ruta="{{route('basepath')}}"></Auth> 
        @endif
    </div>
    @include('sections.script')
</body>
</html>
