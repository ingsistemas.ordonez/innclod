## InncloD Prueba Técnica
Prueba de Jonathan Ordoñez Bello de prueba técnica práctica de PHP para el puesto de Ingeniero de Desarrollo 

## 1. Instalación
Para comenzar a utilizar la prueba, sigue estos pasos:

Clona este repositorio en tu computadora local:

git clone git@gitlab.com:ingsistemas.ordonez/innclod.git

## 2. Abre una terminal en la carpeta del repositorio clonado e instala las dependencias de PHP y JavaScript utilizando Composer y npm:

composer install
npm install

## 3. Cambia el nombre del archivo prueba.env a .env y configura las variables de entorno de la aplicación si son diferentes a las de prueba 


DB_CONNECTION=mysql

DB_HOST=127.0.0.1

DB_PORT=3306

DB_DATABASE=innclod

DB_USERNAME=

DB_PASSWORD=

## 4. Ejecuta las migraciones para crear las tablas de la base de datos, con el siguiente comando creara tambien los registros base:

php artisan migrate

## 5. Genera los archivos CSS y JavaScript de la aplicación utilizando Laravel Mix:

npm run dev

## 6. Iniciar la aplicación utilizando el servidor web incorporado de Laravel:

php artisan serve

## 7. Accede a la aplicación en tu navegador web en la URL http://localhost:8000 o http://127.0.0.1:8000.

## 8. Para el login son las siguientes credenciales soporte@gmail.com : 123456
## Requerimientos del sistema

Para utilizar la prueba, se requiere tener instalado lo siguiente:

- [PHP 7.2 o superior]
- [Composer]
- [MySQL 5.7 o superior]
- [Node.js 10 o superior]
- [npm 6 o superior]

## Dependencias

Utiliza las siguientes dependencias principales:

- [Laravel 7.x]
- [Vue.js 2.x]
- [Bootstrap 5.x]
