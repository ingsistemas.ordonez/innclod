<?php
/* NO ESTÁ EN USO, NO SE ELIMINA MIENTRAS SE AVANZA EN EL PROGRAMA */
namespace app\Http\Handler;
use Illuminate\Http\Request;
use Spatie\WebhookClient\WebhookConfig;
use Symfony\Component\HttpFoundation\Response;
use Spatie\WebhookClient\WebhookResponse\RespondsToWebhook;

class validResponse implements RespondsToWebhook
{
    public function respondToValidWebhook(Request $request, WebhookConfig $config): Response
    {
        //return response()->json(['message' => 'exito']);

        return response()->json(['']);
    }
}
