<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Procesos;

class ProcesosController extends Controller
{
    public function get(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        try {

            $bd = Procesos::get();
            return response()->json($bd, 200);   

        } catch (\Throwable $e) {
            return response()->json(['message' => 'Hubo un error', 'error' => $e ], 401); 
        }
    }

    public function save(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        try {

            $bd = new Procesos;
            $bd->pro_prefijo = strtoupper($request->prefijo);
            $bd->pro_nombre = $request->nombre;
            $bd->save();

            return response()->json(200);   

        } catch (\Throwable $e) {
            return response()->json(['message' => 'Hubo un error', 'error' => $e ], 401); 
        }
    } 

    public function update(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        try {

            $bd = Procesos::find($request->id);
            $bd->pro_prefijo = strtoupper($request->prefijo);
            $bd->pro_nombre = $request->nombre;
            $bd->save();

            return response()->json(200);   

        } catch (\Throwable $e) {
            return response()->json(['message' => 'Hubo un error', 'error' => $e ], 401); 
        }
    }

    public function delete(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        try {

            $bd = Procesos::findOrFail($request->id);
            $bd->delete();

            return response()->json(200);   

        } catch (\Throwable $e) {
            return response()->json(['message' => 'Hubo un error', 'error' => $e ], 401); 
        }
    }
}
