<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Mail\mandrill;

class ResetPasswordController extends Controller
{
    public function resetPassword(Request $request) 
    {
        $password = $request->cPassword;
        if(Auth::user()->pass_temp == "0"){
            return response()->json(['message' => 'El usuario no tiene activa la restauración de la contraseña', 'error' => 'N/A', 'code' => 401 ]); 
        }
        $dataUsers = User::where('id', Auth::user()->id)->where('estado',1)->get();
        if(count($dataUsers) == 0){
            return response()->json(['message' => 'El usuario no tiene activa la restauración de la contraseña', 'error' => 'N/A', 'code' => 401 ]); 
        }
        try {
            $dataArray = [
                'idv' => Auth::user()->id_vtg,
                'id' => Auth::user()->id,
                'name' => Auth::user()->name,
                'email' => Auth::user()->email,
                'password' => bcrypt($password),
                'status' => 0
            ];
            
            $dataResponse = DB::select(
                'call sp_modificar_user(?)',
                [json_encode($dataArray)]
            );
            Auth::user()->pass_temp = "0";

            return response()->json([
                'updatePassword'  => true,
                'code'      => 200
            ]);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Hubo un error al actualizar la contraseña del usuario', 'error' => $e, 'code' => 401 ]); 
        }
    }

    public function sendMail(Request $request){
        $dataUsers = User::where('email', $request->cEmail)->where('estado',1)->get();
        if(count($dataUsers) == 0){
            return response()->json(['message' => 'El correo electronico no se encuentra registrado', 'error' => 'N/A', 'code' => 401 ]); 
        }

        try {
            $password = substr(md5(time()), 0, 10);
            
            $dataArray = [
                'idv' => $dataUsers[0]->id_vtg,
                'id' => $dataUsers[0]->id,
                'name' => $dataUsers[0]->name,
                'email' => $dataUsers[0]->email,
                'password' => bcrypt($password),
                'status' => 1
            ];          
            
            logger("ENVIANDO EMAIL RESETEO CONTRASEÑA");
            $contenido = view('Mails/mailResetPasswordApiChat', ['password' => $password,'url' => 'https://'])->render();
            $adjunto =  ["context" => "", "nombreExcel" => ""];
            $mandrill = new mandrill("n_pagosCampanias", $dataUsers[0]->email, $contenido, "Restablecimiento de contraseña", "RESTABLECIMIENTO DE CONTRASEÑA", $adjunto);
            $mandrill->sendEmail();
            return response()->json([
                'sendMail'  => true,
                'code'      => 200
            ]);
        } catch (\Exception $th) {
            logger('----ERROR CORREO RESETEO CONTRASEÑA -----');
            logger(json_encode($th));
            return response()->json(['message' => 'Hubo un error al resetear la contraseña', 'error' => $th, 'code' => 401 ]); 
        }
        
    }
}
