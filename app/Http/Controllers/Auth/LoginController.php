<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $email    = $request->cEmail;
        $password = $request->cPassword;

        $res = Auth::attempt([ 'email'=>$email, 'password'=>$password, 'estado'=>1 ]); 
    
        if($res) {
            if(Auth::user()->pass_temp == 1){
                return response()->json([
                    'authUser'  => Auth::user(),
                    'code'      => 202
                ]);
            }else{
                return response()->json([
                    'authUser'  => Auth::user(),
                    'code'      => 200
                ]);
            }
        } else {
            return response()->json([
                'code'      => 401
            ]);
        }
    }

    public function show()
    {
        if (Auth::check()) 
            return Redirect::route('dashboard');

        return View::make('login');
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/login');
    }
}