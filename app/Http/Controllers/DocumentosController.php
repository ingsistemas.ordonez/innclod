<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Documentos;

class DocumentosController extends Controller
{
    public function get(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        try {

            $db = Documentos::
            /*select(
                DB::raw("*")
            )*/
            join('pro_proceso', 'pro_proceso.PRO_ID', '=', 'doc_documento.DOC_ID_PROCESO')
            ->join('tip_tipo_doc', 'tip_tipo_doc.TIP_ID', '=', 'doc_documento.DOC_ID_TIPO')
            ->get();  

            return response()->json($db, 200);   

        } catch (\Throwable $e) {
            return response()->json(['message' => 'Hubo un error', 'error' => $e ], 401); 
        }
    }

    private function lastIdCodigo($request) {
        return Documentos::where('DOC_ID_TIPO', $request->tipo_id)
        ->where('DOC_ID_PROCESO', $request->proceso_id)
        ->count() + 1;
    }
    public function save(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        try {

            $latestId = $this->lastIdCodigo($request);

            $bd = new Documentos;
            $bd->doc_codigo = $request->codigo.'-'.$latestId;
            $bd->doc_nombre = $request->nombre;
            $bd->doc_contenido = $request->contenido;
            $bd->doc_id_tipo = $request->tipo_id;
            $bd->doc_id_proceso = $request->proceso_id;
            $bd->save();

            return response()->json(200);   

        } catch (\Throwable $e) {
            return response()->json(['message' => 'Hubo un error', 'error' => $e ], 401); 
        }
    }

    public function update(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        try {

            $latestId = $this->lastIdCodigo($request);

            $bd = new Documentos;
            $bd->doc_codigo = $request->codigo.'-'.$latestId;
            $bd->doc_nombre = $request->nombre;
            $bd->doc_contenido = $request->contenido;
            $bd->doc_id_tipo = $request->tipo_id;
            $bd->doc_id_proceso = $request->proceso_id;
            $bd->save();

            return response()->json(200);   

        } catch (\Throwable $e) {
            return response()->json(['message' => 'Hubo un error', 'error' => $e ], 401); 
        }
    }

    public function delete(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        try {

            $bd = Documentos::findOrFail($request->id);
            $bd->delete();

            return response()->json(200);   

        } catch (\Throwable $e) {
            return response()->json(['message' => 'Hubo un error', 'error' => $e ], 401); 
        }
    }
}
