<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tipos;

class TiposController extends Controller
{
    public function get(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        try {

            $bd = Tipos::get();
            return response()->json($bd, 200);   

        } catch (\Throwable $e) {
            return response()->json(['message' => 'Hubo un error', 'error' => $e ], 401); 
        }
    }

    public function save(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        try {

            $bd = new Tipos;
            $bd->tip_nombre = $request->nombre;
            $bd->tip_prefijo = strtoupper($request->prefijo);
            $bd->save();

            return response()->json(200);   

        } catch (\Throwable $e) {
            return response()->json(['message' => 'Hubo un error', 'error' => $e ], 401); 
        }
    }

    public function update(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        try {

            $bd = Tipos::find($request->id);
            $bd->tip_nombre = $request->nombre;
            $bd->tip_prefijo = strtoupper($request->prefijo);
            $bd->save();

            return response()->json(200);   

        } catch (\Throwable $e) {
            return response()->json(['message' => 'Hubo un error', 'error' => $e ], 401); 
        }
    }

    public function delete(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        try {

            $bd = Tipos::findOrFail($request->id);
            $bd->delete();

            return response()->json(200);   

        } catch (\Throwable $e) {
            return response()->json(['message' => 'Hubo un error', 'error' => $e ], 401); 
        }
    }
}
