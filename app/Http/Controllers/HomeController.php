<?php

namespace App\Http\Controllers;

use App\Documentos;
use App\Procesos;
use App\Tipos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use app\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function valores(Request $request) 
    {
        if(!$request->ajax()) return redirect('/'); 

        $tipos = Tipos::count();
        $procesos = Procesos::count();
        $documentos = Documentos::count();

        return response()->json([
            'tipos' => $tipos,
            'procesos' => $procesos,
            'documentos' => $documentos
        ], 200);  

    }
}
