<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'google_id', 'estado', 'roles_usuario_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array 
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function rol() 
    {
        return $this->belongsTo(RolesUsuario::class, 'roles_usuario_id');
    }

    public function getEstadoAttribute($value)
    {       
        if ($this->preventAttrSet) {
            return $value;
        }

        if($value == 'I')
            return 'Inactivo';
        return 'Activo';
    }
}
