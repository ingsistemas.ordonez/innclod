<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documentos extends Model
{
    protected $table = 'doc_documento';  
    protected $primaryKey = 'DOC_ID';  
    
    public $timestamps = false;

    protected $fillable = [ 
        'doc_nombre', 'doc_codigo', 'doc_contenido'
    ];

    public function proceso() 
    {
        return $this->belongsTo(Procesos::class, 'doc_id_proceso');
    }

    public function tipo() 
    {
        return $this->belongsTo(Tipos::class, 'doc_id_tipo');
    }
} 
