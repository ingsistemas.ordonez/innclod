<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //TODO: ajuste para SSL en DEV y PROD
        if (!empty(getenv('PROXY_URL'))) {
            \URL::forceScheme('https');
        }
    }
}
