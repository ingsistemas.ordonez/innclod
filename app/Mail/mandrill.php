<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Weblee\Mandrill\MandrillFacade;

class mandrill extends Mailable
{
    use Queueable, SerializesModels;

    public  $nomPlantilla;
    public  $tituloCorreo = "API General - Milenium Group";
    public  $asuntoCorreo;
    public  $correoDestino;
    public  $contenidoPlantilla;
    public  $adjuntoCorreo;

    public  $async = false;
    public  $ipPool = null;
    public  $send_at = '';

    public function __construct($nomPlantilla, $correoDestino, $contenidoPlantilla, $asuntoCorreo, $tituloCorreo, $adjuntoCorreo)
    {
        $this->nomPlantilla = $nomPlantilla;
        $this->correoDestino = $correoDestino;
        $this->contenidoPlantilla = $contenidoPlantilla;
        $this->asuntoCorreo = $asuntoCorreo;
        $this->tituloCorreo = $tituloCorreo;
        $this->adjuntoCorreo = $adjuntoCorreo;
    }

    public function build()
    {
    }
    public function sendEmail()
    {
        try {
            if (empty($this->adjuntoCorreo)) {
                $message['attachments'] = null;
            }
            $template_name = $this->nomPlantilla;
            $template_content = array(
                array(
                    "name" => $this->nomPlantilla,
                    "content" => $this->contenidoPlantilla,
                )
            );
            $message = array(
                "subject" => $this->asuntoCorreo,
                "from_email" => "noreply@cellvoz.com.co",
                "from_name" => $this->tituloCorreo,
                "to" => array(
                    array(
                        "email" => $this->correoDestino,
                        "name" => "variable_nombre",
                        "type" => "to"
                    )
                ),
                "headers" => array("Reply-To" => "soporte@cellvoz.com.co"),
                "important" => true,
                "track_opens" => true,
                "merge" => true,
                "merge_language" => "mailchimp",
                "global_merge_vars" => array(
                    array(
                        'name' => 'nombres',
                        'content' => 'nombres'
                    ),
                    array(
                        'name' => 'link',
                        'content' => $this->contenidoPlantilla
                    ),
                    array(
                        'name' => 'titulo',
                        'content' => $this->tituloCorreo
                    ),
                ),
                "merge_vars" => array(
                    array(
                        'rcpt' => '',
                    ),
                    'vars' =>
                    array(
                        array(
                            'name' => 'titulo',
                            'content' => $this->tituloCorreo
                        )
                    )
                ),
                'attachments' => array(
                    array(
                        'content' => $this->adjuntoCorreo['context'],
                        'type' => 'application/xls',
                        'name' => $this->adjuntoCorreo['nombreExcel']
                    )
                ),
                "images" => null
            );

            $resultado_envio = MandrillFacade::messages()->sendTemplate($template_name, $template_content, $message, $this->async, $this->ipPool, $this->send_at);

            logger('-----------------MANDRILL RESULTADO ------------------');
            logger(json_encode($resultado_envio));
            return $resultado_envio;
        } catch (\Throwable  $e) {
            logger('-----------------MANDRILL THROWABLE ------------------');
            logger($e);
            throw $e;
        }
    }
}
