<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Tipos extends Model
{
    protected $table = 'tip_tipo_doc';   
    protected $primaryKey = 'TIP_ID'; 
    
    public $timestamps = false;

    protected $fillable = [ 
        'tip_nombre', 'tip_prefijo'
    ];
} 
