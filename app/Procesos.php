<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Procesos extends Model
{
    protected $table = 'pro_proceso';  
    protected $primaryKey = 'PRO_ID'; 
    
    public $timestamps = false;

    protected $fillable = [ 
        'pro_prefijo', 'pro_nombre'
    ];
} 
