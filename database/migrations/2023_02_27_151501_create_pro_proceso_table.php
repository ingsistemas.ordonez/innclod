<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProProcesoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pro_proceso', function (Blueprint $table) {
            $table->integer('PRO_ID', true);
            $table->string('PRO_PREFIJO', 20);
            $table->string('PRO_NOMBRE', 60);
        });

        DB::table('pro_proceso')->insert([
            [
                'PRO_NOMBRE' => 'Ingeniería',
                'PRO_PREFIJO' => 'ING',
            ],
            [
                'PRO_NOMBRE' => 'Redes',
                'PRO_PREFIJO' => 'RED',
            ],
            [
                'PRO_NOMBRE' => 'Calidad',
                'PRO_PREFIJO' => 'CAL',
            ],
            [
                'PRO_NOMBRE' => 'Compras',
                'PRO_PREFIJO' => 'COM',
            ],
            [
                'PRO_NOMBRE' => 'Recursos Humanos',
                'PRO_PREFIJO' => 'RRHH',
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pro_proceso');
    }
}
