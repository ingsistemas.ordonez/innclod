<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTipTipoDocTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tip_tipo_doc', function (Blueprint $table) {
            $table->integer('TIP_ID', true);
            $table->string('TIP_NOMBRE', 60);
            $table->string('TIP_PREFIJO', 20);
        });

        DB::table('tip_tipo_doc')->insert([
            [
                'TIP_NOMBRE' => 'Procedimiento',
                'TIP_PREFIJO' => 'PRO',
            ],
            [
                'TIP_NOMBRE' => 'Instructivo',
                'TIP_PREFIJO' => 'INS',
            ],
            [
                'TIP_NOMBRE' => 'Manual',
                'TIP_PREFIJO' => 'MAN',
            ],
            [
                'TIP_NOMBRE' => 'Política',
                'TIP_PREFIJO' => 'POL',
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tip_tipo_doc');
    }
}
