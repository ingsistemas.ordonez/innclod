<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocDocumentoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doc_documento', function (Blueprint $table) {
            $table->integer('DOC_ID', true);
            $table->string('DOC_NOMBRE', 60);
            $table->string('DOC_CODIGO', 60);
            $table->text('DOC_CONTENIDO');
            $table->integer('DOC_ID_TIPO')->index('fk_DOC_DOCUMENTO_TIPO_TIPO_DOC_idx');
            $table->integer('DOC_ID_PROCESO')->index('fk_DOC_DOCUMENTO_PRO_PROCESO1_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doc_documento');
    }
}
