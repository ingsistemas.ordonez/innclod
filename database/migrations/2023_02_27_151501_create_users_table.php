<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->integer('id_vtg');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->integer('estado');
        });

        DB::table('users')->insert([
            [
                'name' => 'Juan Perez',
                'email' => 'soporte@gmail.com',
                'id_vtg' => 1,
                'password' => bcrypt('123456'),
                'estado' => 1
            ],
        ]);
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
