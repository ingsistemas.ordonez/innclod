<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToDocDocumentoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('doc_documento', function (Blueprint $table) {
            $table->foreign(['DOC_ID_PROCESO'], 'fk_DOC_DOCUMENTO_PRO_PROCESO1')->references(['PRO_ID'])->on('pro_proceso')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['DOC_ID_TIPO'], 'fk_DOC_DOCUMENTO_TIPO_TIPO_DOC')->references(['TIP_ID'])->on('tip_tipo_doc')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('doc_documento', function (Blueprint $table) {
            $table->dropForeign('fk_DOC_DOCUMENTO_PRO_PROCESO1');
            $table->dropForeign('fk_DOC_DOCUMENTO_TIPO_TIPO_DOC');
        });
    }
}
