<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/ 

$proxy_url    = getenv('PROXY_URL');
$proxy_schema = getenv('PROXY_SCHEMA');

if (!empty(getenv('PROXY_URL'))) {
    URL::forceRootUrl($proxy_url);
}

//Autenticacion de usuario
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout');
Route::post('/resetPassword', 'Auth\ResetPasswordController@sendMail');

//Route::get('auth/google', 'Auth\LoginController@redirectToGoogle');
//Route::get('auth/google/callback', 'Auth\LoginController@handleGoogleCallback');
//Route::get('auth/logout', 'Auth\LoginController@logout');

Route::group(['middleware' => ['auth']], function() {        
    /*Route::get('/', function () {
        return view('app');
    });*/

    Route::get('/logout', 'Auth\LoginController@logout');
    
    Route::get('/dashboard', function () {
        return view('app'); 
    });

    Route::get('/user-login', function () {
        return Auth::user();
    });    

    Route::get('/home/index', 'HomeController@valores');    
    
    Route::get('/procesos/index', 'ProcesosController@get');    
    Route::post('/procesos/save', 'ProcesosController@save');    
    Route::post('/procesos/update', 'ProcesosController@update');  
    Route::post('/procesos/delete', 'ProcesosController@delete');  

    Route::get('/tipos/index', 'TiposController@get');    
    Route::post('/tipos/save', 'TiposController@save');    
    Route::post('/tipos/update', 'TiposController@update');  
    Route::post('/tipos/delete', 'TiposController@delete'); 
    
    Route::get('/documentos/index', 'DocumentosController@get');    
    Route::post('/documentos/save', 'DocumentosController@save');    
    Route::post('/documentos/update', 'DocumentosController@update');  
    Route::post('/documentos/delete', 'DocumentosController@delete'); 
});


/*Route::get('/', function () {
    if (!Auth::check()) {
        return view('app');
    } else {
        Auth::logout();
        return redirect()->route('login');
        //return view('app');
    }
});*/

Route::get('/{optional?}', function () {
    //return view('app');
    if (Auth::check()) {
        return view('app');
    } else {
        //return redirect()->route('/');
        return view('app');
    }
})->name('basepath');

Auth::routes();